
import unittest
import psycopg2
import testing.postgresql
import pandas as pd
from subprocess import PIPE, Popen, DEVNULL
import shlex
from django_harmonization.criteriaset_import import CriteriaSetImporter
import tests
import math
import numpy as np

# from the django_harmonization directory that has the tests dir in it:
#   bash> python -m unittest discover -v 
#   ## error bash> python tests/test_criteriaset_import.py CriterisaSetImportTest.test_add_export_mapping
#   python -m unittest  tests.test_criteriaset_import

ID_RANGE_SIZE=1000000

BIOPORTAL_KEY='16540677-3109-400c-83b7-077af75f9baa'
DBHOST='127.0.0.1'
#DBPORT=9876
DBUSER='postgres'
DBNAME='test'

# for use in bitbucket pipelines
DBPORT=5432

# Houston, we have a problem. Here are two concepts for more/less 'Age', that are both
# labelled as standard in OMOP, but have the same CUI in UMLS.
# The relationships in OMOP don't seem to help: they are different concepts in OMOP.
### AGE_CONCEPT_CODE='424144002'  # WTF: 397669002?
# vocabulary_id |       concept_name        | concept_code |  domain_id  | standard_concept | concept_id
#---------------+---------------------------+--------------+-------------+------------------
# SNOMED        | Age                       | 397669002    | Observation | S                | 4265453
# SNOMED        | Current chronological age | 424144002    | Observation | S                | 4314456
#
# Not in concept_synonyms
# only selves in concept_relationship 
# not obviously related in concept_ancestor

AGE_CONCEPT_CODE='397669002'
AGE_CUI='C0001779'

HDL_CONCEPT_CODE='166832000'
HDL_CUI='C0428472'

GENDER_CONCEPT_CODE = '263495000'
GENDER_CUI='C0079399'
GENDER_CONCEPT_ID=4135376

RACE_CONCEPT_CODE='103579009'
RACE_CONCEPT_ID=4013886
RACE_CUI='C3853635'


MASC_CONCEPT_CODE='703117000'
MASC_CUI='C3839079'
MASC_CONCEPT_ID=45766034

FEM_CONCEPT_CODE ='703118005'
FEM_CUI='C3839293'
FEM_CONCEPT_ID=45766035

WHITE_CUI='C0007457'
WHITE_CONCEPT_CODE='413773004'
WHITE_CONCEPT_ID=4185154

BLACK_CUI='C0085756'
BLACK_CONCEPT_CODE='15086000'
BLACK_CONCEPT_ID=4035165

HISP_CUI='C0086409'
HISP_CONCEPT_CODE='414408004'
HISP_CONCEPT_ID=4188159


DUMPFILE_PATH = "backups/" #study.dump"
TABLE_NAMES = ['study', 'table_column',  'study_mapping_arguments_id_seq', 'study_to_ohdsi_mapping', 
    'study_mapping_arguments', 'categorization_function_metadata', 'categorization_function_parameters', 
    'concept', 'vocabulary_concept', 'extract_study' ]
TABLE_NAMES_FAST = ['study', 'table_column',  'study_mapping_arguments_id_seq', 'study_to_ohdsi_mapping', 
    'study_mapping_arguments', 'categorization_function_metadata', 'categorization_function_parameters', 
    'concept_test', 'vocabulary_concept_test', 'extract_study' ]
# All of those have identical file and table names except concept_test which creates concept 
# and vocabulary_concept_test which creates vocabulary_concept.

# TODO with the rows inserted into map and args tables, test that the keys are in-line and align with the FK relationships:
#   write a joining select to check.
# TODO different race mappings for different studies!, of course 1 with numbers and 1 with strings
criteriaset_rows = [
    # study       form       field  pheno      pheno cui   lower  units    upper  number  string gender rank  cui        string    type
    ['BEST',     'bogus',    'hdl', 'HDL Val', HDL_CUI,    None,  'mg/Dl', None,  0,      None,  None,  None,    None,      'val',    'number'],
    ['BEST',     'patient',  'sex', 'sex',     GENDER_CUI, None,  None,    None,  0,      'm',   None,  1,    MASC_CUI,  'male',   'concept'],
    ['BEST',     'patient',  'sex', 'sex',     GENDER_CUI, None,  None,    None,  0,      'f',   None,  2,    FEM_CUI,   'female', 'concept'],
    ['BEST',     'patient',  'age', 'Age Val', AGE_CUI,    None,  'years', None,  0,      None,  None,  None,    None,      'val',    'number'],
    ['AIM-HIGH', 'labs',     'hdl', 'HDL Val', HDL_CUI,    None,  'mg/Dl', None,  0,      None,  None,  None,    None,      'val',    'number'],
    ['AIM-HIGH', 'patient',  'sex', 'sex',     GENDER_CUI, None,  None,    None,  0,      'm',   None,  1,    MASC_CUI,  'male',   'concept'],
    ['AIM-HIGH', 'patient',  'sex', 'sex',     GENDER_CUI, None,  None,    None,  0,      'f',   None,  2,    FEM_CUI,   'female', 'concept'],
    ['AIM-HIGH', 'patient',  'age', 'Age Val', AGE_CUI,    None,  'years', None,  0,      None,  None,  None,    None,      'val',    'number'],
    ['AIM-HIGH', 'patient',  'age', 'Age Cat', AGE_CUI,    None,  'years', 35,    0,      None,  None,  1,    None,      '< 35',   'number'],
    ['AIM-HIGH', 'patient',  'age', 'Age Cat', AGE_CUI,    35,    'years', 55,    0,      None,  None,  2,    None,      '35-55',  'number'],
    ['AIM-HIGH', 'patient',  'age', 'Age Cat', AGE_CUI,    55,    'years', None,  0,      None,  None,  3,    None,      '> 55',   'number'],
    ['AIM-HIGH', 'bogus',    'race','Race',    RACE_CUI,   None,  None,    None,  1,      None,  None,  1,    WHITE_CUI, 'white',  'concept'],
    ['AIM-HIGH', 'bogus',    'race','Race',    RACE_CUI,   None,  None,    None,  2,      None,  None,  2,    BLACK_CUI, 'black',  'concept'],
    ['AIM-HIGH', 'bogus',    'race','Race',    RACE_CUI,   None,  None,    None,  3,      None,  None,  3,    HISP_CUI, 'hispanic','concept'],
    ['BEST',     'bogus',    'race','Race',    RACE_CUI,   None,  None,    None,  1,      None,  None,  3,    WHITE_CUI, 'white',  'concept'],
    ['BEST',     'bogus',    'race','Race',    RACE_CUI,   None,  None,    None,  2,      None,  None,  2,    BLACK_CUI, 'black',  'concept'],
    ['BEST',     'bogus',    'race','Race',    RACE_CUI,   None,  None,    None,  3,      None,  None,  1,    HISP_CUI, 'hispanic','concept']
]

criteriaset_names = ['Study', 'Data_form', 'Data_Field', 'Phenotype', 'Phenotype_cui', 'Lower_bound', 'Data_Units',
                    'Upper_bound', 'Match_number', 'Match_string', 'Gender_spec', 'Indicates_rank',
                    'Indicates_cui', 'string_value', 'Value_type']

#criteriaset_df = pd.DataFrame(criteriaset_columns)
#criteriaset_df = pd.DataFrame(criteriaset_rows, columns = criteriaset_names)

# FUNCTION INFERENCE:
#  as in the infer_funcion_and_column() function in the CriteriaSetImporter:
#  - non-null values for upper_bound imply ranges_to_rank, though that is a function in the export leg. 
#    On import it implies identity, and a value_as_number column or type.
#  - non-null values for match_string or match_number imply map_string and match_number respectively. 
#    Both have value_as_concept_id type.
#  - null values for all four imply the identity function. IN this case, there isn't enough to identify the 
#    type of the value: string, number or concept.
#
# TYPE INFERENCE: (I hated it in Scala.) 
#   If either match_number or match_string are involved, the stored result is a value_as_concept_id
#   If categorization is invoked by using Upper_bound and lower_bound, the result is usually
#      value_as_number, but can be value_as_concept, depending on Indicates_cui or Indicates_rank 
#   Identity is wide open. It can be either string or number.  THIS NEEDS to be SPECIFED.
#      technically it can be value_as_concept_id. I haven't see it because it implies OMOP concepts in the source.
#
# BEYOND CRITERIASET:
# -Additional Functions-
# The criteriaset data doesn't (as yet, or as far as I understand it) have representations for variables that
# would use the following functions that appear in the mapping specifications developed directly into study_to_ohdsi_mapping.
# - linear_equation This is basically a factor and offset for units conversion in weight and temperature measures.
#   It could evolve into a more domain related imperative method that knows the units and conversions between them
#   obviating the need for specifying the conversion factors.
# - map_concept IS USED?
# - not_null and not_null_number
# - true
#
# -Additional Fields-  in study_to_ohdsi_mapping
# - from_where_clause, from_where_value
#   These columns specifiy a column and a value used to filter the source table rows. from_where_clause
#   could be renamed as from_where_column. They are used in two situations. One is in a long, narrow, 
#   or EAV styled results or lab table. A generic value column is paired with another column that 
#   specifies the lab varialbe the value is associated with. Here, the from_where_value is the name
#   of that variable. Examples of this are in the labs table in HF-ACTION. The values come from the labval
#   column, and the variable is identified in the amcod column. So from_where_column is 'amcod' and
#   from_where_clause has the coded names of the variables.
#   Another situation is to identify the visit from which to choose a result. An example is in the extraction
#   of concomitant meds in ACCORD. We choose values from CONCOMITANTMEDS table when/where the visit value is 
#   'BLR' meaning the baseline visit. In this table, the drugs have their own specific columns.
#   In the future, we may encounter a table that needs where clause elements both for EAV style tables
#   and to specify a visit.
#   Also, note that these columns are included in the foreign key to study_mapping_arguments. This is
#   to distinguish different variables from the same table and column, and do not imply specification of
#   a second  filter.
#
# - units
#   This is just a comment field for units at the present time. It should evolve into a concept that
#   can be used to drive value conversion in place of using the linear_equation function.
#
# - has_date
#   This is a simple way of dealing with which visit to use in value extraction. When this is set to true,
#   rows are sorted according to date, specified in the PERSON class. This is falling into disrepair
#   as the behavior describing studies in the Person class in python has migrated into
#   metadata in the study table, used by a more generic version of the person class. The point of the 
#   migration is to reduce the amount of study-specific code involved, reducing the amount of coding
#   involved in setting up a new study. Back to has_date. It is not used in many studies, since they are
#   driven by analysis tables that don't include values for every visit. It's not always an issue.
#   See issue #186 on BitBucket: 
#     https://bitbucket.org/hfclinicaldata/django_harmonization/issues/186/has_date-needs-a-per-mapping-column
#
# - addl_value_1, addl_column_1 UNUSED, DEPRECATED #187
#     https://bitbucket.org/hfclinicaldata/django_harmonization/issues/187/remove-addl_value_1-addl_column_1
#
# -Additional Fields- in categorization_function_metadat
#   Beyond a place for details about ranges_to_rank, are fields for long_name and short_name
#   They are here so that the names are uniform across an extracted analysis dataset from different input studies.
# 
# -Additiona Functionality-
#  criteriaset does not deal with derived functions, so no metadata for ohdsi_calcuation_function is extracted. 


# https://stackoverflow.com/questions/43380273/pg-dump-pg-restore-password-using-python-module-subprocess
# https://github.com/tk0miya/testing.postgresql

class CriteriaSetImportTest(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.postgresql = testing.postgresql.Postgresql(host=DBHOST, dbname=DBNAME, username=DBUSER, port=DBPORT)
        db_conf = self.postgresql.dsn()
        self.db_con = psycopg2.connect(**db_conf)
        self.db_con.autocommit=True
        self.cur = self.db_con.cursor()

        command = 'psql --host {0} --dbname {1} --username {2} --port {3}'.format(DBHOST, DBNAME, DBUSER, DBPORT )
        command = shlex.split(command)
##      for tablename in TABLE_NAMES:
        for tablename in TABLE_NAMES_FAST:
            with open(DUMPFILE_PATH + tablename + '.dump') as dumpfile:
                p = Popen(command, shell=False, stdin=dumpfile, stdout=DEVNULL, stderr=DEVNULL)
                p.wait()
                dumpfile.close()

    #def setUp(self): print("CASE SETUP")

    #def tearDown(self): print("CASE DOWN")
    @classmethod
    def tearDownClass(self):
        self.cur.close()    
        self.postgresql.stop()


    def test_find_vocab_concept_from_cui(self):
        importer = CriteriaSetImporter(self.db_con)
        (vocab, concept)= importer.find_vocab_concept_from_cui(GENDER_CUI)
        assert vocab == 'SNOMED'
        assert concept == GENDER_CONCEPT_CODE

    def test_get_study_id(self): 
        importer = CriteriaSetImporter(self.db_con)
        id =  importer.get_study_id('BEST');
        assert id == 1 

    def test_create_and_get_study_id_1(self):
        importer = CriteriaSetImporter(self.db_con)
        ## before, should not find it
        id =  importer.get_study_id('TEST_STUDY')
        assert id == None

        ## should create a new study with id 99
        importer.create_new_study("TEST_STUDY", 99)

        ## should find it
        id =  importer.get_study_id('TEST_STUDY');
        assert id == 99

        ## should not have an issue if it's already been created
        importer.create_new_study("TEST_STUDY", 99)

        ## should find it
        id =  importer.get_study_id('TEST_STUDY');
        assert id == 99
       
    def test_find_max_study_id(self): 
        importer = CriteriaSetImporter(self.db_con)
        importer.create_new_study("TEST_STUDY", 99)
        id = importer.find_max_study_id()
        assert id == 99

    def test_find_domain_from_vocab_concept(self):
        importer = CriteriaSetImporter(self.db_con)
        domain = importer.find_domain_from_vocab_concept('SNOMED', GENDER_CONCEPT_CODE)
        assert domain == 'observation'

        domain = importer.find_domain_from_vocab_concept('SNOMED', HDL_CONCEPT_CODE)
        assert domain == 'measurement'

    def test_infer_function_and_to_column_identity_1_meas_none(self):
        importer = CriteriaSetImporter(self.db_con)
        (f,c) = importer.infer_function_and_column('measurement', None, None, None, None)
        assert f == 'identity'
        assert c == 'value_as_number'

    def test_infer_function_and_to_column_identity_1_obs(self):
        importer = CriteriaSetImporter(self.db_con)
        (f,c) = importer.infer_function_and_column('observation', None, None, None, None)
        assert f == 'identity'
        assert c == 'value_as_string' 

    def test_infer_function_and_to_column_identity_2(self):
        importer = CriteriaSetImporter(self.db_con)
        (f,c) = importer.infer_function_and_column('no matter', 1, 2, None, None)
        assert f == 'identity'
        assert c == 'value_as_string' or c == 'value_as_number'

    def test_infer_function_and_to_column_match_number(self):
        importer = CriteriaSetImporter(self.db_con)
        (f,c) = importer.infer_function_and_column('no matter', None, None, 1, None)
        assert f == 'map_number'
        assert c == 'value_as_concept_id'


    def test_infer_function_and_to_column_match_string(self):
        importer = CriteriaSetImporter(self.db_con)
        (f,c) = importer.infer_function_and_column('no matter', None, None, None, 'x')
        assert f == 'map_string'
        assert c == 'value_as_concept_id'

        


    def test_put_study_mapping_row(self):
        # clear out exisiting mappings that came in from the dump file
        stmt = 'delete from study_to_ohdsi_mapping;';
        self.cur.execute(stmt);

        importer = CriteriaSetImporter(self.db_con)
        correct=[1, 'bogus', 'hdl', 'identity', 'SNOMED', HDL_CONCEPT_CODE, 'measurement', 'value_as_number', None, None, 'mg/Dl', False]
        (study_id, from_table, from_column, function_name, vocabulary_id, concept_code,
        to_table, to_column, from_where_clause, from_where_column, units, has_date) = correct

        importer.put_study_mapping_row(study_id, from_table, from_column, function_name, vocabulary_id, concept_code,
            to_table, to_column, from_where_clause, from_where_column, units, has_date)
        row = importer.get_study_mapping_row(study_id, 'bogus', 'hdl');

        if row != (study_id, from_table, from_column, function_name, vocabulary_id, concept_code,
            to_table, to_column, from_where_clause, from_where_column, units, has_date):
            print("ROW:", row)
            print("COR:", (study_id, from_table, from_column, function_name, vocabulary_id, concept_code,
            to_table, to_column, from_where_clause, from_where_column, units, has_date))
        assert row == (study_id, from_table, from_column, function_name, vocabulary_id, concept_code,
            to_table, to_column, from_where_clause, from_where_column, units, has_date)

    def test_add_mapping_rows(self):
        # KEY is (Study, Data_form, Data_filed, Phenotype)
        
        # clear out exisiting mappings that came in from the dump file
        stmt = 'delete from study_to_ohdsi_mapping;';
        self.cur.execute(stmt);

        importer = CriteriaSetImporter(self.db_con)

        df = pd.DataFrame(criteriaset_rows, columns=criteriaset_names)

        # functions, types and columns remain BOGUS
        # study, table, column, function, vocab, concept, table, column, where_clause, where_column, units. has_date
        correct  = {
            ('AIM-HIGH', 'patient', 'sex'):(22, 'patient', 'sex',  'map_string', 'SNOMED', GENDER_CONCEPT_CODE, 'observation', 'value_as_concept_id', None, None, None, False),
            ('AIM-HIGH', 'patient', 'age'):(22, 'patient', 'age',  'identity',   'SNOMED', AGE_CONCEPT_CODE,    'observation', 'value_as_number',     None, None, 'years', False),
            ('AIM-HIGH', 'bogus', 'race') :(22, 'bogus',   'race', 'map_number', 'SNOMED', RACE_CONCEPT_CODE,   'observation', 'value_as_concept_id', None, None, None, False),
            ('AIM-HIGH', 'labs', 'hdl'):   (22, 'labs',    'hdl',  'identity',   'SNOMED', HDL_CONCEPT_CODE,    'measurement', 'value_as_number',     None, None, 'mg/Dl', False),
            ('BEST', 'patient', 'sex'):    (1,  'patient', 'sex',  'map_string', 'SNOMED', GENDER_CONCEPT_CODE, 'observation', 'value_as_concept_id', None, None, None, False),
            ('BEST', 'patient', 'age'):    (1,  'patient', 'age',  'identity',   'SNOMED', AGE_CONCEPT_CODE,    'observation', 'value_as_number',     None, None, 'years', False),
            ('BEST', 'bogus', 'race'):     (1,  'bogus',   'race', 'map_number', 'SNOMED', RACE_CONCEPT_CODE,   'observation', 'value_as_concept_id', None, None, None, False),
            ('BEST', 'bogus', 'hdl'):      (1,  'bogus',   'hdl',  'identity',   'SNOMED', HDL_CONCEPT_CODE,    'measurement', 'value_as_number',     None, None, 'mg/Dl', False)
        }

        for i in range(0, len(df)):
            hash_row = df.iloc[i]
            importer.add_study_mapping_row(hash_row)
            key = (hash_row[0], hash_row[1], hash_row[2])
            row = importer.get_study_mapping_row(correct[key][0], correct[key][1], correct[key][2])
            assert row != None
            if row != correct[key]:
                print("ROW:", row)
                print("COR:", correct[key])
            assert row == correct[key]

    def test_add_mapping_arguments(self): 
        # TODO: looks like Dave uses match_string in all cases, never match_number??   COULD BE THAT TYPING ISSUE@!@!! TODO
        df = pd.DataFrame(criteriaset_rows, columns=criteriaset_names)

        # None values in the match_number column come back as Nan and cause problems when used as a key:
        # https://stackoverflow.com/questions/45300367/why-adding-multiple-nan-in-python-dictionary-giving-multiple-entries

        # clear out exisiting mappings that came in from the dump file
        stmt = 'delete from study_to_ohdsi_mapping;';
        self.cur.execute(stmt);
        stmt = 'delete from study_mapping_arguments;';
        self.cur.execute(stmt);

        # RUN
        importer = CriteriaSetImporter(self.db_con)
        for i in range(0, len(df)):
            hash_row = df.iloc[i]
            importer.add_study_mapping_arguments(hash_row)
        # GET RESULTS
        rows = importer.get_all_study_mapping_argument_rows()
        # EXPECT:
        #          study form     field   number string  function       where where fac shift  vocab    concept
        # dict lookups with Nan are problematic, use 0 instead here for getting the right comparison row.
        correct = {
            (22,'patient', 'age',  0,    None) :  ('identity',   None, None, 1.0, 0.0, None, None),
            (22,'patient', 'sex',  0,    'm')  :  ('map_string', None, None, 1.0, 0.0, 'SNOMED', MASC_CONCEPT_CODE),
            (22,'patient', 'sex',  0,    'f')  :  ('map_string', None, None, 1.0, 0.0, 'SNOMED', FEM_CONCEPT_CODE),
            (22,'labs',    'hdl',  0,    None) :  ('identity',   None, None, 1.0, 0.0, None, None),
            (22,'bogus', 'race',   2,    None) :  ('map_number', None, None, 1.0, 0.0, 'SNOMED', BLACK_CONCEPT_CODE),
            (22,'bogus', 'race',   3,    None) :  ('map_number', None, None, 1.0, 0.0, 'SNOMED', HISP_CONCEPT_CODE),
            (22,'bogus', 'race',   1,    None) :  ('map_number', None, None, 1.0, 0.0, 'SNOMED', WHITE_CONCEPT_CODE),
            (22, 'bogus', 'hdl', 0, None): ('identity', None, None, 1.0, 0.0, None, None),

            (1,'patient', 'age',   0,    None) :  ('identity',   None, None, 1.0, 0.0, None, None),
            (1,'patient', 'sex',   0,    'm' ) :  ('map_string', None, None, 1.0, 0.0, 'SNOMED', MASC_CONCEPT_CODE),
            (1,'patient', 'sex',   0,    'f' ) :  ('map_string', None, None, 1.0, 0.0, 'SNOMED', FEM_CONCEPT_CODE),
            (1,'bogus',    'hdl',  0,    None) :  ('identity',   None, None, 1.0, 0.0, None, None),
            (1, 'bogus', 'race',   2,    None) :  ('map_number', None, None, 1.0, 0.0, 'SNOMED', BLACK_CONCEPT_CODE),
            (1, 'bogus', 'race',   3,    None) :  ('map_number', None, None, 1.0, 0.0, 'SNOMED', HISP_CONCEPT_CODE),
            (1, 'bogus', 'race',   1,    None) :  ('map_number', None, None, 1.0, 0.0, 'SNOMED', WHITE_CONCEPT_CODE),
            }
#        ('AIM-HIGH', 'labs', 'hdl'): ( 22, 'labs', 'hdl', 'identity', 'SNOMED', HDL_CONCEPT_CODE, 'measurement', 'value_as_number', None, None, 'mg/Dl', False),
        # value of 8th field is irrelevant, can come up NaN, when a None comes in : Pandas dataframe?

        # CHECK
        for row in rows:
            (study_id, from_table, from_column, mapped_number, mapped_string) = \
                (row[0], row[1], row[2], row[7], row[6])
            key = (study_id, from_table, from_column, mapped_number, mapped_string)
            values =  row[3:6] + row[8:12]
            correct_values = correct[key]
            assert correct_values is not None
            if correct_values != values:
                print("key:", key, "cor:", correct_values, "val:", values)
            assert correct_values == values

    def test_db_is_there_in_dev(self):
        for table_name in TABLE_NAMES:
            if table_name != 'study_mapping_arguments_id_seq':
                self.cur.execute("""select table_name   from information_schema.tables 
                            where table_schema = 'public' and table_name = '{0}';""".format(table_name))
                l = len(self.cur.fetchall()) 
                if l != 1:
                    print("no table:", table_name)
                assert l == 1

    def test_add_export_mapping(self):
        stmt = 'delete from categorization_function_metadata;'
        self.cur.execute(stmt);
        stmt = 'delete from categorization_function_parameters;'
        self.cur.execute(stmt);

        # TODO CHRIS FIX!!!!! there should only be a single export mapping from the two studies and merged, provided the input data was merged as it should be

        # key is: (extract_study_id, function_name, long_name, rule_id, from_vocabulary_id, from_concept_code)
        # (extract_study_id, function_name, long_name, rule_id, from_vocabulary_id, from_concept_code, comment, from_table, short_name)
        """ 
            extract_study_id is some standard conversion of the study_id
            function_name could be map_concept, identity, ranges_to_rank....
            long_name is phenotype
            rule_id is ?
            from_vocabulary_id, from_concept_code is ?
            comment is ignored
            from_table is from the domain_id
            short_name?
        """
        df = pd.DataFrame(criteriaset_rows, columns=criteriaset_names)
        importer = CriteriaSetImporter(self.db_con)
        for i in range(0,len(df)):
            hash_row = df.iloc[i]
            importer.add_export_mapping_rows(hash_row)

            extract_study_id = importer.get_study_id(hash_row['Study']) + 1000
            function_name = 'identity'
            long_name = hash_row['Phenotype']
            cui = hash_row['Phenotype_cui']
            if long_name == 'sex':
                function_name = 'map_string'
            rule_id = '1'
            (vocab, concept)  = importer.find_vocab_concept_from_cui(cui)
            rows = importer.get_export_mapping(extract_study_id, function_name, long_name, rule_id, vocab, concept)
            i=0
            correct = [ #study function  phenotype  rule  vocab      concept       comment  table          short      seq
                (100001, 'identity',     'HDL Val', '1', 'SNOMED', HDL_CONCEPT_CODE,    '', 'measurement', 'HDL Val', None),
                (100001, 'map_string',   'sex',     '1', 'SNOMED', GENDER_CONCEPT_CODE, '', 'observation', 'sex',     None),
                (100001, 'map_string',   'sex',     '1', 'SNOMED', GENDER_CONCEPT_CODE, '', 'observation', 'sex',     None),
                (100001, 'identity',     'Age Val', '1', 'SNOMED', AGE_CONCEPT_CODE,    '', 'observation', 'Age Val', None),

                (100022, 'identity',     'HDL Val', '1', 'SNOMED', HDL_CONCEPT_CODE,    '', 'measurement', 'HDL Val', None),
                (100022, 'map_string',   'sex',     '1', 'SNOMED', GENDER_CONCEPT_CODE, '', 'observation', 'sex',     None),
                (100022, 'map_string',   'sex',     '1', 'SNOMED', GENDER_CONCEPT_CODE, '', 'observation', 'sex',     None),
                (100022, 'identity',     'Age Val', '1', 'SNOMED', AGE_CONCEPT_CODE,    '', 'observation', 'Age Val', None),
                (100022, 'identity',     'Age Cat', '1', 'SNOMED', AGE_CONCEPT_CODE,    '', 'observation', 'Age Cat', None),
                (100022, 'identity',     'Age Cat', '1', 'SNOMED', AGE_CONCEPT_CODE,    '', 'observation', 'Age Cat', None),
                (100022, 'identity',     'Age Cat', '1', 'SNOMED', AGE_CONCEPT_CODE,    '', 'observation', 'Age Cat', None),
                (100022, 'map_number',   'Race',    '1', 'SNOMED', RACE_CONCEPT_CODE,   '', 'observation', 'Race',    None),
                (100022, 'map_number',   'Race',    '1', 'SNOMED', RACE_CONCEPT_CODE,   '', 'observation', 'Race',    None),
                (100022, 'map_number',   'Race',    '1', 'SNOMED', RACE_CONCEPT_CODE,   '', 'observation', 'Race',    None),

                (100001, 'map_number',   'Race',    '1', 'SNOMED', RACE_CONCEPT_CODE,   '', 'observation', 'Race',    None),
                (100001, 'map_number',   'Race',    '1', 'SNOMED', RACE_CONCEPT_CODE,   '', 'observation', 'Race',    None),
                (100001, 'map_number',   'Race',    '1', 'SNOMED', RACE_CONCEPT_CODE,   '', 'observation', 'Race',    None),
            ]
            for r in rows:
                if r != correct[i]:
                    print("EXPORT ROW:", i, r)
                    print("EXPORT COR:", correct[i])
                assert r == correct[i]
                i += 1

    def test_add_export_mapping_arguments(self):
        df = pd.DataFrame(criteriaset_rows, columns=criteriaset_names)
        importer = CriteriaSetImporter(self.db_con)
        for i in range(0,len(df)):
            hash_row = df.iloc[i]
            importer.add_export_mapping_arguments(hash_row)

        #                study     function      long   rule value_limit    rank from_string from_concept_id
        correct = {
            'HDL Val': [(100022, 'identity',   'HDL Val', '1', None,            1,    None, None)],
            'sex'    : [(100022, 'map_string', 'sex',     '1', np.float('nan'), 1,    'm',  MASC_CONCEPT_ID),
                        (100022, 'map_string', 'sex',     '1', np.float('nan'), 2,    'f',  FEM_CONCEPT_ID)],
            'Age Val': [(100022, 'identity',   'Age Val', '1', None,            None, None, None)],
            'Age Cat': [(100022, 'identity',   'Age Cat', '2', 35,              1,    None, None),
                        (100022, 'identity',   'Age Cat', '2', 55,              2,    None, None),
                        (100022, 'identity',   'Age Cat', '2', 0,               3,    None, None)],
            'Race': [   (100022, 'map_number', 'Race',    '1', None,            1,    None, WHITE_CONCEPT_ID),
                        (100022, 'map_number', 'Race',    '1', None,            2,    None, BLACK_CONCEPT_ID),
                        (100022, 'map_number', 'Race',    '1', None,            3,    None, HISP_CONCEPT_ID)]
        }
        # the nan's come from Pandas, which is created by the np.float() function and  you get a different one each time.
        # this means you can't compare them for equality, philosophical reasons aside.
        for longname in correct:
            for correct_row in correct[longname]:
                if correct_row[3] is math.nan:
                    rows = importer.get_export_mapping_arguments(correct_row[0], correct_row[1], correct_row[2], None, correct_row[5])
                else:
                    rows = importer.get_export_mapping_arguments(correct_row[0], correct_row[1], correct_row[2], correct_row[3], correct_row[5])
                i = 0
                # which is the right concept?
                # None goes in Nan comes back for
                for r in rows:
                    if r[3] is math.nan:
                        if r[0:4] != correct_row[0:4]:
                            print("fail export mapping args row:", r[0:4])
                            print("fail export mapping args cor:", correct_row[0:4])
                        assert r[0:4] == correct_row[0:4]
                        if r[5:] != correct_row[3:]:
                            print("fail export mapping args row2:", r[5:])
                            print("fail export mapping args cor2:", correct_row[5:])
                        assert  r[5:] == correct_row[5:]
                    else:
                        if r[0:4] != correct_row[0:4]:
                            print("fail export mapping args row3:", r[0:4])
                            print("fail export mapping args cor3:", correct_row[0:4])
                        assert r[0:4] == correct_row[0:4]
                        if  r[5:] != correct_row[5:]:
                            print("fail export mapping args row4:", r[5:])
                            print("fail export mapping args cor4:", correct_row[5:])
                            print(longname)
                            print(r)
                            print("cor", correct_row)
                        assert r[5:] == correct_row[5:]


#importer.add_export_mapping_arguments(criteria_row#)def test_compare_mapping_rows(a, b):
#    (study_id, from_table, from_column, function, vocabulary_id, concept_code, to_table, to_column,
#     from_where_clause, from_where_column,
#     units, has_date)

if __name__ == '__main__' :
    #unittest.main()
    unittest.main(tests.test_criteriaset_import)












































