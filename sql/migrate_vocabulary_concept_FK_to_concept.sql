
create index idx_concept_vocabulary_id_concept_code on concept (vocabulary_id, concept_code);

-- v3 had a separate table: vocabulary_concept
alter    TABLE "categorization_function_table" drop CONSTRAINT if exists "categorization_function_table_from_vocabulary_id_fkey" ;
alter    TABLE "ohdsi_calculation_argument"    drop CONSTRAINT if exists "ohdsi_calculation_argument_to_vocabulary_id_fkey" ;
alter    TABLE "ohdsi_calculation_argument"    drop CONSTRAINT if exists "ohdsi_calculation_argument_vocabulary_id_fkey" ;
alter    TABLE "ohdsi_calculation_function"    drop CONSTRAINT if exists "ohdsi_calculation_function_to_vocabulary_id_fkey"; 
alter    TABLE "study_to_ohdsi_mapping"        drop CONSTRAINT if exists "study_to_ohdsi_mapping_vocabulary_id_fkey" ;


alter    TABLE "categorization_function_table" add CONSTRAINT 
"FK_categorization_function_table_from_vocabulary_id" 
foreign key (from_vocabulary_id, from_concept_code) references concept (vocabulary_id, concept_code) ;

alter    TABLE "ohdsi_calculation_argument"    add CONSTRAINT 
"FK_ohdsi_calculation_argument_to_vocabulary_id" 
foreign key (to_vocabulary_id, to_concept_code) references concept (vocabulary_id, concept_code) ;

alter    TABLE "ohdsi_calculation_argument"    add CONSTRAINT 
"FK_ohdsi_calculation_argument_vocabulary_id" 
foreign key (vocabulary_id, concept_code) references concept (vocabulary_id, concept_code) ;

alter    TABLE "ohdsi_calculation_function"    add CONSTRAINT 
"FK_ohdsi_calculation_function_to_vocabulary_id" 
foreign key (to_vocabulary_id, to_concept_code) references concept (vocabulary_id, concept_code) ;

alter    TABLE "study_to_ohdsi_mapping"        add CONSTRAINT 
"FK_study_to_ohdsi_mapping_vocabulary_id" 
foreign key (vocabulary_id, concept_code) references concept (vocabulary_id, concept_code) ;


