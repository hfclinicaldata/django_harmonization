-- Domain: "UCD.Kao Domain"
-- Vocabulary: "UCD.Kao Vocabulary"
-- Concept Class: "UCD.Kao Concept Class"

ALTER TABLE concept DROP CONSTRAINT fpk_concept_domain ;
ALTER TABLE concept DROP CONSTRAINT fpk_concept_class ;
ALTER TABLE concept DROP CONSTRAINT fpk_concept_vocabulary;

ALTER TABLE vocabulary DROP CONSTRAINT fpk_vocabulary_concept ;
ALTER TABLE domain DROP CONSTRAINT fpk_domain_concept ;
ALTER TABLE concept_class DROP CONSTRAINT fpk_concept_class_concept ;

insert into cdm.concept values (2000000101, 'UCD.Kao Concept Class Concept', 'UCD.Kao D', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-101', '2018-04-13', '2099-12-31', null  );
insert into cdm.concept values (2000000102, 'UCD.Kao Vocabulary Concept',    'UCD.Kao D', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-102', '2018-04-13', '2099-12-31', null  );
insert into cdm.concept values (2000000103, 'UCD.Kao Domain Concept',        'UCD.Kao D', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-103', '2018-04-13', '2099-12-31', null  );

insert into concept_class values('UCD.Kao CC', 'UCD.Kao Concept Class', 2000000101);
insert into vocabulary    values('UCD.Kao', 'UCD.Kao Vocabulary', null, null,     2000000102);
insert into domain        values('UCD.Kao D', 'UCD.Kao Domain',         2000000103);

insert into cdm.concept values (2000000001, 'Heart failure duration', 'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-1', '2018-04-13', '2099-12-31', null  );
insert into cdm.concept values (2000000002, 'Pump Failure',           'UCD.Kao D', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-2', '2018-04-13', '2099-12-31', null  );
insert into cdm.concept values (2000000003, 'Cardiovascular Death',   'UCD.Kao D', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-3', '2018-04-13', '2099-12-31', null  );
insert into cdm.concept values (2000000004, 'Non CV Death',           'UCD.Kao D', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-4', '2018-04-13', '2099-12-31', null  );
insert into cdm.concept values (2000000005, 'End of Study',           'UCD.Kao D', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-5', '2018-04-13', '2099-12-31', null  );
insert into cdm.concept values (2000000006, 'Randomization',          'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-6', '2018-04-13', '2099-12-31', null  );
insert into cdm.concept values (2000000007, 'Aggregated Cardia Arrest',    'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-7', '2018-04-13', '2099-12-31', null);
insert into cdm.concept values (2000000008, 'Fatal Myocardial Infarction', 'UCD.Kao D', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-8', '2018-04-13', '2099-12-31', null);
insert into cdm.concept values (2000000009, 'Other CV Death',              'UCD.Kao D', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-9', '2018-04-13', '2099-12-31', null); 
insert into cdm.concept values (2000000010, 'Uknown cause of death',       'UCD.Kao D', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-10','2018-04-13', '2099-12-31', null); 
insert into cdm.concept values (2000000011, 'Ethnicity Unknown',            'Race',     'SNOMED',    'Social Context', null, '10241000175103', '2017-09-14', '2099-12-13', null); 

insert into cdm.concept values (2000000012, 'paradigm.ctd_flg1',          'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-12','2018-04-13', '2099-12-31', null); 

insert into cdm.concept values (2000000901, 'A',          'UCD.Kao TEST', 'UCD.Kao', 'UCD.Kao CC', null, 'test-1','2018-07-06', '2099-12-31', null); 
insert into cdm.concept values (2000000902, 'B',          'Measurement', 'UCD.Kao', 'UCD.Kao CC', null, 'test-2','2018-07-06', '2099-12-31', null); 
insert into cdm.concept values (2000000903, 'C',          'UCD.Kao TEST', 'UCD.Kao', 'UCD.Kao CC', null, 'test-3','2018-07-06', '2099-12-31', null); 
insert into cdm.concept values (2000000904, 'D',          'Measurement', 'UCD.Kao', 'UCD.Kao CC', null, 'test-4','2018-07-06', '2099-12-31', null); 
insert into cdm.concept values (2000000905, 'E',          'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'test-5','2018-07-06', '2099-12-31', null); 
insert into cdm.concept values (2000000906, 'sex',        'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'test-6','2018-07-06', '2099-12-31', null); 

-- use SNOMED 69031000119105
-- insert into cdm.concept values (2000000013, 'paradigm.crt_flg1',          'UCD.Kao D', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-13','2018-04-13', '2099-12-31', null); 

insert into cdm.concept values (2000000014, 'paradigm.icd_flg1',          'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-14', '2018-04-13', '2099-12-31', null); 
insert into cdm.concept values (2000000015, 'paradigm.ang_flg1',          'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-15', '2018-04-13', '2099-12-31', null); 
insert into cdm.concept values (2000000016, 'paradigm.angioten',          'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-16', '2018-04-13', '2099-12-31', null); 
insert into cdm.concept values (2000000017, 'Alternate Patient ID',       'UCD.Kao D',   'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-17', '2018-04-13', '2099-12-31', null);
insert into cdm.concept values (2000000018, 'Death Days',                 'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-18', '2018-04-13', '2099-12-31', null);
insert into cdm.concept values (2000000019, 'Tmt is placebo/std of care', 'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-19', '2018-04-13', '2099-12-31', null); 
insert into cdm.concept values (2000000020, 'End Of Study Days',          'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-20', '2018-04-13', '2099-12-31', null);
insert into cdm.concept values (2000000021, 'End Of Study Date',          'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-21', '2018-04-13', '2099-12-31', null);
insert into cdm.concept values (2000000022, 'Topcat End Of Study years',  'UCD.Kao D',   'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-22', '2018-04-13', '2099-12-31', null);
insert into cdm.concept values (2000000023, 'Topcat Site death years',    'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-23', '2018-04-13', '2099-12-31', null);
insert into cdm.concept values (2000000024, 'Topcat CEC death years',     'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-24', '2018-04-13', '2099-12-31', null);
insert into cdm.concept values (2000000025, 'Topcat Site vs CEC',         'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-25', '2018-04-13', '2099-12-31', null);
insert into cdm.concept values (2000000026, 'Death Days',                 'UCD.Kao D',   'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-26', '2018-04-13', '2099-12-31', null);
insert into cdm.concept values (2000000027, 'Death Status',               'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-27', '2018-10-25', '2099-12-31', null); 
insert into cdm.concept values (2000000028,'CV Death Status',             'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-28', '2018-10-25', '2099-12-31', null);
insert into cdm.concept values (2000000029, 'CV Death days',              'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-29', '2018-10-25', '2099-12-31', null);
insert into cdm.concept values (2000000030, 'All-cause Hospital status',  'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-30', '2018-10-25', '2099-12-31', null);
insert into cdm.concept values (2000000031, 'All-cause Hospital days',    'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-31', '2018-10-25', '2099-12-31', null);
insert into cdm.concept values (2000000032, 'CV Hospital status',         'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-32', '2018-10-25', '2099-12-31', null);
insert into cdm.concept values (2000000033, 'CV Hospital days',           'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-33', '2018-10-25', '2099-12-31', null);
insert into cdm.concept values (2000000034, 'HF Hospital status',         'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-34', '2018-10-25', '2099-12-31', null);
insert into cdm.concept values (2000000035, 'ACCORD CaCB',                'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-35', '2018-10-25', '2099-12-31', null);
insert into cdm.concept values (2000000036, 'CV Death or non-fatal MI or non-fatal stroke','Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-36', '2018-10-25', '2099-12-31', null);
insert into cdm.concept values (2000000037, 'CORONA 4-value Tobacco Use', 'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-37', '2018-10-28', '2099-12-31', null);
insert into cdm.concept values (2000000038, 'original_subject_id',        'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-38', '2018-12-05', '2099-12-31', null);
insert into cdm.concept values (2000000039, 'Relative Wall Thickness',    'Measurement', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-39', '2018-12-05', '2099-12-31', null);
insert into cdm.concept values (2000000040, '2nd DBP',                    'Measurement', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-40', '2018-12-05', '2099-12-31', null);
insert into cdm.concept values (2000000041, '2nd SBP',                    'Measurement', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-41', '2018-12-05', '2099-12-31', null);
insert into cdm.concept values (2000000042, 'dSBP',                       'Measurement', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-42', '2019-04-04', '2099-12-31', null); --
insert into cdm.concept values (2000000043, 'dDBP',                       'Measurement', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-43', '2019-04-04', '2099-12-31', null); --
insert into cdm.concept values (2000000051, 'EPV EMS Ratio DRV',          'UCD.Kao D',   'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-51', '2019-01-17', '2099-12-31', null);
insert into cdm.concept values (2000000053, 'BSA DRV',                    'UCD.Kao D',   'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-53', '2019-01-17', '2099-12-31', null); 
insert into cdm.concept values (2000000054, 'RWT Cat',                    'Measurement', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-54', '2019-02-06', '2099-12-31', null); --
insert into cdm.concept values (2000000055, 'LVM Cat',                    'Measurement', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-55', '2019-02-06', '2099-12-31', null); --
insert into cdm.concept values (2000000056, 'LVM Cat male',               'Measurement', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-56', '2019-02-06', '2099-12-31', null); --
insert into cdm.concept values (2000000057, 'LVM Cat female',             'Measurement', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-57', '2019-02-06', '2099-12-31', null); --
insert into cdm.concept values (2000000058, 'HF Hospital days',           'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-58', '2018-10-25', '2099-12-31', null);
insert into cdm.concept values (2000000059, 'Gender Cat',                 'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-59', '2019-02-06', '2099-12-31', null);
insert into cdm.concept values (2000000060, 'LVH Cat',                    'Measurement', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-60', '2019-02-19', '2099-12-31', null); --
insert into cdm.concept values (2000000061, 'BARI-2D non sublingal nitrate',       'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-61', '2019-04-05', '2099-12-31', null);
insert into cdm.concept values (2000000062, 'ACCORD Other BP med',        'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-62', '2019-04-05', '2099-12-31', null);
insert into cdm.concept values (2000000063, 'MI Stroke',                  'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-63', '2019-04-16', '2099-12-31');
insert into cdm.concept values (2000000064, 'Race 2 class',               'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-64', '2019-04-19', '2099-12-31');
insert into cdm.concept values (2000000065, 'Race Non-African American',  'UCD.Kao D',   'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-65', '2019-04-24', '2099-12-31');
insert into cdm.concept values (2000000066, 'Study Name',                 'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-66', '2019-05-08', '2099-12-31');
insert into cdm.concept values (2000000067, 'Member of ALLHAT LLT tmt?',  'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-67', '2019-05-15', '2099-12-31');
insert into cdm.concept values (2000000068, 'Member of ALLHAT DOX tmt?',  'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-68', '2019-05-15', '2099-12-31');
insert into cdm.concept values (2000000069, 'AIM-HIGH 4-value Tobacco Use','Observation','UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-69', '2019-05-15', '2099-12-31');

insert into cdm.concept values (2000000104, 'On ACE/ARB',                 'Observation','UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-104', '2020-01-23', '2099-12-31');
insert into cdm.concept values (2000000105, 'On MRA',                     'Observation','UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-105', '2020-01-23', '2099-12-31');
insert into cdm.concept values (2000000106, 'On long-acting nitrate',     'Observation','UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-106', '2020-01-23', '2099-12-31');

insert into cdm.concept values (2000000110, 'RV end diastolic area',     'Observation','UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-110', '2020-01-23', '2099-12-31');
insert into cdm.concept values (2000000111, 'RV function',               'Observation','UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-111', '2020-01-23', '2099-12-31');
insert into cdm.concept values (2000000112, 'RV function topcat tertile','Observation','UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-112', '2020-01-23', '2099-12-31');

insert into cdm.concept values (2000000115, 'Serum eGMP',                'Observation','UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-115', '2020-01-23', '2099-12-31');
insert into cdm.concept values (2000000116, 'Treatment group',           'Observation','UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-116', '2020-01-23', '2099-12-31');
insert into cdm.concept values (2000000117, 'E/e prime average',         'Observation','UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-117', '2020-01-23', '2099-12-31');
insert into cdm.concept values (2000000118, 'E/e prime lateral ',        'Observation','UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-118', '2020-01-23', '2099-12-31');
insert into cdm.concept values (2000000119, 'On long-acting nitrate',    'Observation','UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-119', '2020-01-23', '2099-12-31');

insert into cdm.concept values (2000000120, 'LA end systolic area',      'Observation','UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-120', '2020-01-23', '2099-12-31');
insert into cdm.concept values (2000000121, 'LA volume index',           'Observation','UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-121', '2020-01-23', '2099-12-31');
insert into cdm.concept values (2000000123, 'LVEDD index',               'Observation','UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-123', '2020-01-23', '2099-12-31');
insert into cdm.concept values (2000000124, 'LVESD index',               'Observation','UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-124', '2020-01-23', '2099-12-31');
insert into cdm.concept values (2000000125, 'E/e prime septal',          'Observation','UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-125', '2020-01-23', '2099-12-31');
insert into cdm.concept values (2000000126, 'Echo done',                 'Observation','UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-126', '2020-01-23', '2099-12-31');
insert into cdm.concept values (2000000127, 'Global longitudinal strain','Observation','UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-127', '2020-01-23', '2099-12-31');


insert into cdm.concept values(2000000300, 'Stage 4/5',   'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-300', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000301, 'Quartile 1',  'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-301', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000302, 'Quartile 2',  'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-302', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000303, 'Quartile 3',  'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-303', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000304, 'Quartile 4',  'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-304', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000305, 'Tertile 1',   'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-305', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000306, 'Tertile 2',   'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-306', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000307, 'Tertile 3',   'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-307', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000308, '1.Normal',    'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-308', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000309, '2.Grade I',   'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-309', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000310, '3.Grade II',  'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-310', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000311, '4.Grade III', 'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-311', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000312, '1.Normal',    'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-312', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000313, '2.Abnormal',  'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-313', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000314, '1.Normal',    'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-314', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000315, '2.Elevated',  'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-315', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000316, '1.Normal',    'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-316', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000317, '2.Indeterminate', 'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-317', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000318, '3.Elevated',  'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-318', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000319, '1.Normal',    'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-319', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000320, '2.Mild',      'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-320', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000321, '3.Moderate',  'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-321', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000322, '4.Severe',    'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-322', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000323, '1.Normal',    'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-323', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000324, '2.Decreased', 'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-324', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000325, '3.Increased', 'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-325', '2020-06-25', '2099-12-31');

insert into cdm.concept values(2000000326, '220+',        'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-326', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000327, '150-220',     'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-327', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000328, '<150',        'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-328', '2020-06-25', '2099-12-31');

insert into cdm.concept values(2000000329, '1.Normal/Mild',     'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-329', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000330, '2.Moderate/Severe', 'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-330', '2020-06-25', '2099-12-31');

insert into cdm.concept values(2000000331, '<30',         'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-331', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000332, '30-40',       'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-332', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000333, '40+',         'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-333', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000334, '140+',        'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-334', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000335, '130-140',     'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-335', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000336, '<130',        'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-336', '2020-06-25', '2099-12-31');

insert into cdm.concept values(2000000337, '<1 year',     'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-337', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000338, '1-5 years',   'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-338', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000339, '5+ years',    'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-339', '2020-06-25', '2099-12-31');

insert into cdm.concept values(2000000340, '<120',        'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-340', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000341, '>120',        'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-341', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000342, 'Hypertension','Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-342', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000343, 'Normal',      'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-343', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000344, 'Hypotension', 'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-344', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000345, '<60',         'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-345', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000346, '60-80',       'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-346', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000347, '80+',         'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-347', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000348, '<60',         'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-348', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000349, '60-80',       'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-349', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000350, '80+',         'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-350', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000378, '80-100',      'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-378', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000379, '100+',        'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-379', '2020-06-25', '2099-12-31');

insert into cdm.concept values(2000000351, '< 50',        'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-351', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000352, '50-75',       'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-352', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000353, '50+',         'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-353', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000354, '1. Americas', 'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-354', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000355, '2. Russian republics', 'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-355 ', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000356, 'New onset',   'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-356', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000357, 'LCZ696',      'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-357', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000358, 'Group 1',     'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-358', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000359, 'Group 2',     'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-359', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000360, 'Intervention 2', 'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-360', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000361, 'Intervention', 'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-361', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000362, 'Low dose carvedilol', 'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-362', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000363, 'Medium dose carvedilol', 'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-363', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000364, 'High dose carvedilol', 'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-364', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000365, 'I-II',        'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-365', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000366, 'III-IV',      'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-366', '2020-06-25', '2099-12-31');

insert into cdm.concept values(2000000367, 'Noninsulin dependent', 'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-367', '2020-06-25', '2099-12-31');

insert into cdm.concept values(2000000368, '< 55',        'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-368', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000369, '55-75',       'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-369', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000370, '75+',         'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-370', '2020-06-25', '2099-12-31');

insert into cdm.concept values(2000000371, 'E prime lateral', 'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-140', '2020-06-25', '2099-12-31');

insert into cdm.concept values(2000000372, 'Bucindolol',  'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-372', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000373, 'Hispanic',    'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-373', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000374, 'Standard of Care',    'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-374', '2020-06-25', '2099-12-31');

insert into cdm.concept values(2000000375, '< 120',       'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-375', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000376, '120-150',     'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-376', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000377, '150+',        'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-377', '2020-06-25', '2099-12-31');

insert into cdm.concept values(2000000380, '< 15',        'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-380', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000381, '15-25',       'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-381', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000382, '25+',         'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-382', '2020-06-25', '2099-12-31');

insert into cdm.concept values(2000000383, '1. Normal',         'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-383', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000384, '2. Enlarged',         'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-384', '2020-06-25', '2099-12-31');

insert into cdm.concept values(2000000385, '1. Normal',         'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-385', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000386, '2. Mild/mod+',         'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-386', '2020-06-25', '2099-12-31');
insert into cdm.concept values(2000000387, '3. Severe',         'Observation', 'UCD.Kao', 'UCD.Kao CC', null, 'UCD-Kao-387', '2020-06-25', '2099-12-31');

ALTER TABLE concept ADD CONSTRAINT fpk_concept_domain FOREIGN KEY (domain_id)  REFERENCES domain (domain_id);
ALTER TABLE concept ADD CONSTRAINT fpk_concept_class FOREIGN KEY (concept_class_id)  REFERENCES concept_class (concept_class_id);
ALTER TABLE concept ADD CONSTRAINT fpk_concept_vocabulary FOREIGN KEY (vocabulary_id)  REFERENCES vocabulary (vocabulary_id);
ALTER TABLE concept_class ADD CONSTRAINT fpk_concept_class_concept FOREIGN KEY (concept_class_concept_id)  REFERENCES concept (concept_id);
ALTER TABLE domain ADD CONSTRAINT fpk_domain_concept FOREIGN KEY (domain_concept_id)  REFERENCES concept (concept_id);
ALTER TABLE vocabulary ADD CONSTRAINT fpk_vocabulary_concept FOREIGN KEY (vocabulary_concept_id)  REFERENCES concept (concept_id);
