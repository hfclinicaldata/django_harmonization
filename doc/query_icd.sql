-- select * from concept 
-- where concept_code in ('Z95.5', 'I70.2', 'I70', 'I25', 'R01', 'I21','R80.3');

select ca.vocabulary_id, ca.concept_code, substr(ca.concept_name,0,80), substr(cd.concept_name,0,80) from concept_ancestor a 
join concept cd on cd.concept_id = a.descendant_concept_id
join concept ca on ca.concept_id = a.ancestor_concept_id
where ca.concept_code in ('R01','Z95.5', 'I70.2', 'I70', 'I25', 'I21','R80.3')
; -- and max_levels_of_separation < 4;

select ca.vocabulary_id, ca.concept_code, substr(ca.concept_name,0,80), substr(cd.concept_name,0,80) from concept_ancestor a 
join concept cd on cd.concept_id = a.descendant_concept_id
join concept ca on ca.concept_id = a.ancestor_concept_id
where cd.concept_code in ('R01', 'Z95.5', 'I70.2', 'I70', 'I25', 'I21','R80.3')
; -- and max_levels_of_separation < 4;

-- select * from concept_relationship
-- where concept_id_1, cocnept_id_2
--  ndconcept_code in ('Z95.5', 'I70.2', 'I70', 'I25',  'I21','R80.3');

-- empty table
-- select * from concept_synonym concept_id language_concept_id
