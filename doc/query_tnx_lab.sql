
-- TNX:LAB:9001 HDL
-- -----------------------------------
select 'TNX:LAB:9001 HDL' as concept;
-- select substr(concept_name,0,80), vocabulary_id, concept_code 
-- select count(*)
-- from concept where (concept_name like '%HDL%' and concept_name not like '%MHDL%') or concept_name ilike '%high density lipoprotein%' and standard_concept is not null;
select concept_name, concept_code, standard_concept 
from concept where concept_code in ('18263-4', '49130-8', '2085-9');

-- select distinct c.vocabulary_id,c.concept_code, c.concept_name 
select c.vocabulary_id,c.concept_code, substr(c.concept_name,0,60), count(distinct ca.concept_code)
from concept_ancestor a 
join concept c on c.concept_id = a.ancestor_concept_id 
join concept ca on ca.concept_id = a.descendant_concept_id
where ca.concept_code in ('18263-4', '49130-8', '2085-9')
and c.standard_concept = 'S'
group by c.vocabulary_id, c.concept_code, substr(c.concept_name, 0, 60)
having count(distinct ca.concept_code) > 1;



-- TNX:LAB:9002 LDL
-- -----------------------------------
select 'TNX:LAB:9002 LDL' as concept;
-- select substr(concept_name,0,80), vocabulary_id, concept_code 
-- select count(*)
-- from concept where (concept_name like '%LDL%' and concept_name not like '%MHDL%') or concept_name ilike '%low density lipoprotein%' and standard_concept is not null;
select concept_name, concept_code, standard_concept 
from concept where concept_code in ('13457-7', '2089-1', '18261-8', '49132-4', '18262-6');


-- select distinct c.vocabulary_id,c.concept_code, c.concept_name
select c.vocabulary_id,c.concept_code, substr(c.concept_name,0,60), count(distinct ca.concept_code)
from concept_ancestor a 
join concept c on c.concept_id = a.ancestor_concept_id 
join concept ca on ca.concept_id = a.descendant_concept_id
where ca.concept_code in ('13457-7', '2089-1', '18261-8', '49132-4', '18262-6')
and c.standard_concept = 'S'
group by c.vocabulary_id, c.concept_code, substr(c.concept_name, 0, 60)
having count(distinct ca.concept_code) > 1;

-- TNX:LAB:9003 BNP
-- -----------------------------------
select 'TNX:LAB:9003 BNP' as concept;
-- select substr(concept_name,0,80), vocabulary_id, concept_code 
-- select count(*)
-- from concept where (concept_name like '%BNP%') or concept_name ilike '%brain%peptide%' and standard_concept is not null;
select concept_name, concept_code, standard_concept 
from concept where concept_code in ('30934-4', '42637-9');


-- select distinct c.vocabulary_id,c.concept_code, c.concept_name 
select c.vocabulary_id,c.concept_code, substr(c.concept_name,0,60), count(distinct ca.concept_code)
from concept_ancestor a 
join concept c on c.concept_id = a.ancestor_concept_id 
join concept ca on ca.concept_id = a.descendant_concept_id
where ca.concept_code in ('30934-4', '42637-9')
and c.standard_concept = 'S'
group by c.vocabulary_id, c.concept_code, substr(c.concept_name, 0, 60)
having count(distinct ca.concept_code) > 1;

-- TNX:LAB:9004 TG
-- -----------------------------------
select 'TNX:LAB:9004 TG' as concept;
-- select substr(concept_name,0,80), vocabulary_id, concept_code 
-- select count(*)
-- from concept where  concept_name ilike '%triglyceride%' and standard_concept is not null;
select concept_name, concept_code, standard_concept 
from concept where concept_code in ('12951-0', '2571-8', '3043-7');

-- select distinct c.vocabulary_id,c.concept_code, c.concept_name 
select c.vocabulary_id,c.concept_code, substr(c.concept_name,0,60), count(distinct ca.concept_code)
from concept_ancestor a 
join concept c on c.concept_id = a.ancestor_concept_id 
join concept ca on ca.concept_id = a.descendant_concept_id
where ca.concept_code in ('12951-0', '2571-8', '3043-7')
and c.standard_concept = 'S'
group by c.vocabulary_id, c.concept_code, substr(c.concept_name, 0, 60)
having count(distinct ca.concept_code) > 1;

-- TNX:LAB:9037 A1c
-- -----------------------------------
select 'TNX:LAB:9037 A1c' as concept;
-- select substr(concept_name,0,80), vocabulary_id, concept_code 
-- select count(*)
-- from concept where concept_name ilike '%A1c%' and standard_concept is not null;
select concept_name, concept_code, standard_concept 
from concept where concept_code in ('4548-4', '17855-8', '17856-6', '4549-2', '62388-4', '59261-8');

-- select distinct c.vocabulary_id,c.concept_code, c.concept_name 
select c.vocabulary_id,c.concept_code, substr(c.concept_name,0,60), count(distinct ca.concept_code)
from concept_ancestor a 
join concept c on c.concept_id = a.ancestor_concept_id 
join concept ca on ca.concept_id = a.descendant_concept_id
where ca.concept_code in ('4548-4', '17855-8', '17856-6', '4549-2', '62388-4', '59261-8')
and c.standard_concept = 'S'
group by c.vocabulary_id, c.concept_code, substr(c.concept_name, 0, 60)
having count(distinct ca.concept_code) > 1;

-- TNX:LAB:9081 body weight
-- -----------------------------------
select 'TNX:LAB:9081 body weight' as concept;
-- select substr(concept_name,0,80), vocabulary_id, concept_code 
-- select count(*)
-- from concept where concept_name ilike '%body weight%' and standard_concept is not null;
select concept_name, concept_code, standard_concept 
from concept where concept_code in ('8335-2', '3141-9', '3142-7', '29463-7');


-- select distinct c.vocabulary_id,c.concept_code, c.concept_name 
select c.vocabulary_id,c.concept_code, substr(c.concept_name,0,60), count(distinct ca.concept_code)
from concept_ancestor a 
join concept c on c.concept_id = a.ancestor_concept_id 
join concept ca on ca.concept_id = a.descendant_concept_id
where ca.concept_code in ('8335-2', '3141-9', '3142-7', '29463-7')
and c.standard_concept = 'S'
group by c.vocabulary_id, c.concept_code, substr(c.concept_name, 0, 60)
having count(distinct ca.concept_code) > 1;

-- TNX:LAB:9083 BMI
-- -----------------------------------
select 'TNX:LAB:9083 BMI' as concept;
-- select substr(concept_name,0,80), vocabulary_id, concept_code 
-- select count(*)
-- from concept where (concept_name like '%BMI%' and concept_name not like '%MHDL%') or concept_name ilike '%body mass index%' and standard_concept is not null;
select concept_name, concept_code, standard_concept 
from concept where concept_code in  ('39156-5');


-- select distinct c.vocabulary_id,c.concept_code, c.concept_name 
select c.vocabulary_id,c.concept_code, substr(c.concept_name,0,60), count(distinct ca.concept_code)
from concept_ancestor a 
join concept c on c.concept_id = a.ancestor_concept_id 
join concept ca on ca.concept_id = a.descendant_concept_id
where ca.concept_code in ('39156-5')
and c.standard_concept = 'S'
group by c.vocabulary_id, c.concept_code, substr(c.concept_name, 0, 60)
having count(distinct ca.concept_code) > 1;

-- TNX:LAB:9085 SBP
-- -----------------------------------
select 'TNX:LAB:9085 SBP' as concept;
-- select substr(concept_name,0,80), vocabulary_id, concept_code 
-- select count(*)
-- from concept where (concept_name like '%SBP%' and concept_name not like '%MHDL%') or concept_name ilike '%systolic blood pressure%' and standard_concept is not null;
-- standard concepts
select concept_name, concept_code, standard_concept 
from concept
where concept_code in ('8460-8', '8461-6', '8459-0', '8450-9', '87741-5', '8480-6', '84535-5', '75997-7', '8451-7', '76215-3', '8479-8', '87739-9') ;


-- select distinct c.vocabulary_id,c.concept_code, c.concept_name 
select c.vocabulary_id,c.concept_code, substr(c.concept_name,0,60), count(distinct ca.concept_code)
from concept_ancestor a 
join concept c on c.concept_id = a.ancestor_concept_id 
join concept ca on ca.concept_id = a.descendant_concept_id
where ca.concept_code in ('8460-8', '8461-6', '8459-0', '8450-9', '87741-5', '8480-6', '84535-5', '75997-7', '8451-7', '76215-3', '8479-8', '87739-9') 
and c.standard_concept = 'S'
group by c.vocabulary_id, c.concept_code, substr(c.concept_name, 0, 60)
having count(distinct ca.concept_code) > 1;

-- TNX:LAB:9086 DBP
-- -----------------------------------
select 'TNX:LAB:9086 DBP' as concept;
-- select substr(concept_name,0,80), vocabulary_id, concept_code 
-- select count(*)
-- from concept where (concept_name like '%DBP%' and concept_name not like '%MHDL%') or concept_name ilike '%diastolic blood pressure%' and standard_concept is not null;
select concept_name, concept_code, standard_concept 
from concept where concept_code in ( '76535-4', '8447-5', '8453-4', '87740-7', '8462-4', '75995-1', '8455-8', '76213-8', '87736-5', '8454-2', '8446-7');


-- select distinct c.vocabulary_id,c.concept_code, substr(c.concept_name,0,60) as ancestor, substr(ca.concept_name,0,60) as descendant, ca.concept_code
select c.vocabulary_id,c.concept_code, substr(c.concept_name,0,60), count(distinct ca.concept_code)
from concept_ancestor a 
join concept c on c.concept_id = a.ancestor_concept_id 
join concept ca on ca.concept_id = a.descendant_concept_id
where ca.concept_code in ( '76535-4', '8447-5', '8453-4', '87740-7', '8462-4', '75995-1', '8455-8', '76213-8', '87736-5', '8454-2', '8446-7')
and c.standard_concept = 'S'
group by c.vocabulary_id, c.concept_code, substr(c.concept_name, 0, 60)
having count(distinct ca.concept_code) > 1;




