#!/usr/bin/env python3

# Criteria Set Import:
# This script imports modified Dave's criteria set format into Chris's OMOP mapping tables.
# The modification is done with a script, add_phenotypes.py that needs a phenotypes file...
#
# criteriaset_import.py <file>
#  reads the named file and writes mappings to the OMOP harmonization mapping tables based
#    on the study_names in the file.
#  Provisionally cretaes new study entries to avoid over-writing existing configurations.
#  Conntects to the database indicated by PG environment variables PGDATABASE, PGHOST, PGPORT, etc.
#
# The primary challenge beyond mapping is dealing with the splits between mapping and argument/parameter
#   tables, and the splits between import and export/categorization. Not all the data from the file
#   goes into study_to_ohdsi_mapping or even study_mapping_arguments.
#
# Chris Roeder 10/2019

# FYI: on pandas dataframe iteration:
# https://stackoverflow.com/questions/16476924/how-to-iterate-over-rows-in-a-dataframe-in-pandas/41022840#41022840

#

# Study,Data_form,Data_Field,Phenotype,Phenotype_cui,Lower_bound,Data_Units,Upper_bound,Match_number,Match_string,Gender_spec,Indicates_rank,string_value
# AIM-HIGH,patient,age,Age category,\N,  \N,\N,55,\N,\N,\N,1,< 55
# AIM-HIGH,patient,age,Age category,\N,  55,\N,75,\N,\N,\N,2,55-75

# CriteriaSet: Study, Data_form,Data_Field,Phenotype,Phenotype_cui, Lower_bound,Data_Units, Upper_bound,
#              Match_number, Match_string, Gender_spec, Indicates_rank, string_value
#
# study_to_ohdsi_mapping: study_no, from_table, from_column, function_name,
#             vocabulary_id, concept_code, to_table (deprecated), to_column,
#             addl_value_1, addl1_column_1, from_where_clause, comment,
#             from_where_column, units, has_date, id
#
#    Study        --> study_id
#      names in criteriaset, numbers in OMOP. Join through the study table.
#    Data_form         --> from_table
#
#    Data_field        --> from_column
#
#    Phenotype    --> (short_name in categorization_function_metadata)
#      n/a
#    Phenotype_cui          -->  (to_vocabulary_id, to_concept_code)
#
#    Lower_bound  --> (see upperbound & categorization_function_parameters.value_limit)
#
#    Data_units        --> study_to_ohdsi_mapping.units
#
#    upper_bound  --> categorization_function_parameters.value_limit
#
#    Match_number --> study_mapping_arguments.mapped_number
#
#    Match_string --> study_mapping_arguments.mapped_string (categorization_mapping_arguments.from_string??)
#
#    Gender_spec  --> study_mapping.from_where_clause and study_mapping.from_where_column
#
#    Indicates_rank         --> categorization_mapping_arguments.rank
#
#    string_value --> Q:Is this a name for the rank?
#
# ADDED FIELDS:
#    Indicates_cui
#    Value_type
#
#


import argh
import psycopg2
import logging
import pandas
import os

import urllib.request, urllib.error, urllib.parse
import json
import math


# DataFrames munge ints into numpy.int64 which psycopg doesn't know about: educate it.
# https://stackoverflow.com/questions/50626058/psycopg2-cant-adapt-type-numpy-int64
# Worse, PyCharm complains about the AsIs import, but it seems to work.
import numpy as np
import psycopg2.extensions
psycopg2.extensions.register_adapter(np.int64, psycopg2._psycopg.AsIs)


class CriteriaSetImporter:


    # UMLS and OMOP have different names for the vocabularies!
    CRITERIA_SET_STUDY_OFFSET = 1000
    ID_RANGE_SIZE=1000000
    EXTRACT_ID_OFFSET=100000

    def __init__(self, con):
        self._con = con
        self._cur = con.cursor()
        self._vocab_name_map = { 'SNOMEDCT':'SNOMED' }
        self._domain_table_map = {'Observation':'observation', 'Procedure':'procedure_occurrence',
            'Measurement':'measurement', 'Condition':'condition_occurrence'}
        self._type_map = { 'number':'value_as_number', 'concept':'value_as_concept_id', 'string':'value_as_string'}
        self._mapping_keys = {}
        self._categorization_keys = {}

    def find_vocab_concept_from_cui(self, cui):
        #
        # TODO make work for more than SNOMED
        #
        API_KEY='16540677-3109-400c-83b7-077af75f9baa'
        BIOPORTAL_URL='http://data.bioontology.org'
        url = BIOPORTAL_URL + "/search?q=" + cui
        opener = urllib.request.build_opener()
        opener.addheaders = [('Authorization', 'apikey token=' + API_KEY)]
        json_data = json.loads(opener.open(url).read())
        vocab=''
        concept_id=''
        for i in range(0, len(json_data["collection"])):
            # http://<org>/ontology/<vocab>/<code>
            parts=json_data["collection"][i]['@id'].split("/", 10)
            if parts[3] == 'ontology':
                if parts[4] in self._vocab_name_map:
                    vocab=self._vocab_name_map[parts[4]]
                concept_id=parts[5]
                if (parts[4] == 'SNOMEDCT'):
                    return (vocab, concept_id)

    def find_domain_from_vocab_concept(self, vocab, concept):
        stmt = "select domain_id from concept where vocabulary_id = %s and concept_code = %s";
        self._cur.execute(stmt, (vocab, concept))
        rows = self._cur.fetchall()
        if len(rows) < 1 or len(rows[0]) < 1:
            print("**NO CONCEPT**", vocab, concept)
            return None
        else:
            if rows[0][0] in self._domain_table_map:
                return self._domain_table_map[rows[0][0]]
            else:
                return rows[0][0]



    def find_max_study_id(self):
        study_query = "select max(study_id) from study;";
        self._cur.execute(study_query)
        file_rows = self._cur.fetchall()
        return file_rows[0][0]

    def get_study_id(self, study_name):
        """ This fucntion looks up the study name """
        study_query = "select study_id from study where study_name = %s ";
        self._cur.execute(study_query, (study_name,))
        file_rows = self._cur.fetchall()
        if file_rows:
            return file_rows[0][0]
        else:
            return None

    def get_study_mapping_row(self, study_id, from_table, from_column):

        study_query = ("select study_id, from_table, from_column, function_name, "
        "vocabulary_id, concept_code, to_table, to_column, from_where_clause,"
        " from_where_column, units, has_date from study_to_ohdsi_mapping"
        "  where study_id = %s and from_table = %s and from_column = %s; ");
        self._cur.execute(study_query, (study_id, from_table, from_column))
        file_rows = self._cur.fetchall()
        if file_rows:
            return file_rows[0]
        else:
            return None

    def create_new_study(self, study_name, id):
        study_id = self.get_study_id(study_name)
        if study_id is None:
            stmt = ("insert into study (study_id, study_name, person_id_range_start,"
                    " person_id_range_end, loaded, migrated, calculated, study_class)"
                    " values (%s, %s, %s, %s, 'f', 'f', 'f', 'ParametricPerson')")
            try:
                self._cur.execute(stmt, (id, study_name, id * self.ID_RANGE_SIZE, (id+1) * self.ID_RANGE_SIZE))
            except:
                print("....seen that study before, nevermind")

    def lookup_by_cui(self, cui):
        (vocabulary_id, concept_code) = (None, None)
        to_table = None
        if cui is not None:
            if ((type(cui) == type("s")) or not math.isnan(cui)):
                concept_parts = self.find_vocab_concept_from_cui(cui)
                if concept_parts is not None:
                    (vocabulary_id, concept_code) = concept_parts
                    to_table = self.find_domain_from_vocab_concept(vocabulary_id, concept_code)
        return (vocabulary_id, concept_code, to_table)

    # STUDY MAPPING
    def put_study_mapping_row(self, study_id, from_table, from_column, function_name, vocabulary_id,
                              concept_code, to_table, to_column, from_where_clause, from_where_column, units, has_date) :
        study_query = ("insert into study_to_ohdsi_mapping "
                   "(study_id, from_table, from_column, function_name, "
                   "vocabulary_id, concept_code, to_table, to_column, "
                   "from_where_clause, from_where_column, units, has_date)"
                   " values (%s, %s, %s, %s,  %s, %s, %s, %s,  %s, %s, %s, %s) ");
        try:
            self._cur.execute(study_query, (study_id, from_table, from_column, function_name, vocabulary_id, concept_code,
                to_table, to_column, from_where_clause, from_where_column, units, has_date) )
        except e as Exception:
            print("....seen that study before, nevermind")


    def infer_function_and_column(self, to_table, lower_bound, upper_bound, match_number, match_string):
        """ This function attempts to decide where to put the data in OMOP based on the metadata we have.
            It assumes irrelevant input variables have values of None in the metadata.
            @param to_table helps (imperfectly?) discern the data column.
            @param lower_bound says this is a number to be catorized.
            @param upper_bound  "
            @param match_number says this is a number to be classified as a concept
            @param match_string says this is a string to be classified as a concept
            @returns (function, to_column) function is one of identity, map_string or map_Number. The bounds are
                used in exporting categorized values, not on import as needed here. The numerical values this implies
                just come in on identity.

            The to_column value is/should-be in the categorization-data. The inference is left here
            for QC.
        """
        function=''
        to_column=''

        # bounds ==> identity  and later categorization
        if ((lower_bound is not None  or upper_bound is not None) and (not math.isnan(lower_bound) or not math.isnan(upper_bound))) :
            # categorize ON EXPORT, identity for now
            function = 'identity'
            if to_table == 'measurement':
                to_column = 'value_as_number'
            else:
                to_column = 'value_as_string' # HOW TO FIGURE THIS OUT?

        # match_number ==> map_number
        #elif match_number is not None and not math.isnan(match_number):
        elif match_number is not None and match_number != 0:
            function='map_number'
            to_column = 'value_as_concept_id'

        # match_string ==> map_strin
        elif match_string is not None:
            function='map_string'
            to_column = 'value_as_concept_id'

        # no bounds, no matches ==> identity, the normal way  you would expect it.
        elif (lower_bound is None  and upper_bound is None) \
            and match_number is None \
            and match_string is None  :
            # categorize ON EXPORT, identity for now here on import
            function = 'identity'
            if to_table == 'measurement':
                to_column = 'value_as_number'
            else: # (observation or other...)
                to_column = 'value_as_string' # HOW TO FIGURE THIS OUT?
        else:
            function = 'identity'
            if to_table == 'measurement':
                to_column = 'value_as_number'
            else: # (observation or other...)
                to_column =  'value_as_string'  #OUCH

        #print("INFER ",  to_table, lower_bound, upper_bound, match_number, match_string, function, to_column)
        return (function, to_column)

    def compare_study_mapping_rows(self, a, b):
        good = True

        for i in range(0, 12): # CHRIS WTF
            if a[i] != b[i]:
                good = False
        return good

    def add_study_mapping_row(self,  criteria_row):
        #    1.Study             --> study_id
        #    2.Data_form         --> from_table
        #    3.Data_field        --> from_column
        #    7.Data_units        --> study_to_ohdsi_mapping.units
        #    5.Phenotype_cui     -->  (to_vocabulary_id, to_concept_code)
        study_id = self.get_study_id(criteria_row['Study'])
        key = ( study_id,
                criteria_row['Data_form'],
                criteria_row['Data_Field'],
                criteria_row['Phenotype'],
                criteria_row['Phenotype_cui']  )
        if (study_id is not None):
            if key not in self._mapping_keys:
                self._mapping_keys[key]=None

                # find to_table from domain_id in OMOP
                (vocabulary_id, concept_code, to_table) = self.lookup_by_cui(criteria_row['Phenotype_cui'])
                (function, to_column) = self.infer_function_and_column(
                    to_table,
                    criteria_row['Lower_bound'], criteria_row['Upper_bound'],
                    criteria_row['Match_number'], criteria_row['Match_string'])
                to_column = self._type_map[criteria_row['Value_type']]; # infer

                prior = self.get_study_mapping_row(study_id, criteria_row['Data_form'], criteria_row['Data_Field'])
                if prior is not None:
                    if not self.compare_study_mapping_rows(
                            (study_id, criteria_row['Data_form'], criteria_row['Data_Field'],
                             function, vocabulary_id, concept_code, to_table, to_column, None, None,
                             criteria_row['Data_Units'], 'f'),
                            prior):
                        print("add_study_mapping_row:", study_id, criteria_row['Data_form'], criteria_row['Data_Field'],
                              function, vocabulary_id, concept_code, to_table, to_column, None, None, criteria_row['Data_Units'], 'f',)

                        print("add_study_mapping_row prior", prior)

                else:
                    self.put_study_mapping_row(study_id, criteria_row['Data_form'], criteria_row['Data_Field'],
                        function, vocabulary_id, concept_code, to_table, to_column, None, None,
                        criteria_row['Data_Units'], 'f')
            #else:
            #    print("ALREADY have key:", key, "...skipping")
        else:
            print("ERRROR null id for study {} ".format(criteria_row['Study'][0]))
        return key

# STUDY MAPPING ARGUMENTS
    def get_study_mapping_argument_rows(self, study_id):
        study_query = ("select study_id, from_table, from_column, function_name,"
                       "  from_where_clause, from_where_column, "
                       "mapped_string, mapped_number, transform_factor, "
                       "transform_shift, to_concept_vocabulary_id, to_concept_code"
                       " from study_mapping_arguments where study_id = %s; ");
        self._cur.execute(study_query, (study_id,))
        file_rows = self._cur.fetchall()
        if file_rows:
            return file_rows
        else:
            return None

    def get_all_study_mapping_argument_rows(self):
        study_query = ("select study_id, from_table, from_column, function_name,"
                       "  from_where_clause, from_where_column, "
                       "mapped_string, mapped_number, transform_factor, "
                       "transform_shift, to_concept_vocabulary_id, to_concept_code"
                       " from study_mapping_arguments;");
        self._cur.execute(study_query)
        file_rows = self._cur.fetchall()
        if file_rows:
            return file_rows
        else:
            return None

    def add_study_mapping_arguments(self, criteria_row):
        # study_id, from_table, from_column, function_name, from_where_clause, from_where_column,
        # mapped_string, mapped_number, transform_factor, transform_shift, to_concept_vocabulary_id, to_concept_code

        study_id = self.get_study_id(criteria_row['Study'])

        stmt = ("insert into study_mapping_arguments "
                "(study_id, from_table, from_column, function_name, from_where_clause, "
                " from_where_column, mapped_string, mapped_number, transform_factor, "
                " transform_shift, to_concept_vocabulary_id, to_concept_code)"
                " values (%s, %s, %s, %s, %s, %s,   %s, %s, %s, %s, %s, %s);")

        (vocabulary_id, concept_code, to_table) = self.lookup_by_cui(criteria_row['Indicates_cui'])
        (function, to_column) = self.infer_function_and_column(
            to_table,
            criteria_row['Lower_bound'], criteria_row['Upper_bound'],
            criteria_row['Match_number'], criteria_row['Match_string'])
        to_column = self._type_map[criteria_row['Value_type']]; # infer
        self._cur.execute(stmt, (study_id, criteria_row['Data_form'], criteria_row['Data_Field'],
               function,  ##'identity',
                None, None,
                criteria_row['Match_string'],
                criteria_row['Match_number'],
                1.0, 0, vocabulary_id, concept_code) )

# EXPORT MAPPING
    # extract_study: extract_study_id, study_id, name, comment
    def get_extract_study(self, extract_study_id):
        stmt = "select extract_study_id, study_id, name, comment from extract_study where extract_study_id = %s";
        self._cur.execute(stmt, (extract_study_id,))
        rows = self._cur.fetchall()
        if rows is not None and len(rows) > 0:
            return rows[0]
        else:
            return None

    def create_extract_study(self, extract_study_id, name):
        stmt=("insert into extract_study (extract_study_id, study_id, name, comment)"
              " values (%s, %s, %s, %s)")
        self._cur.execute(stmt, (extract_study_id, 0, name, ""))


    def get_export_mapping(self, extract_study_id, function_name, long_name, rule_id, from_vocabulary_id,
                           from_concept_code):
        stmt = ("select extract_study_id, function_name, long_name, rule_id, from_vocabulary_id, "
                "from_concept_code, comment, from_table, short_name, sequence "
                "from categorization_function_metadata "
                " where extract_study_id = %s and  function_name = %s and long_name = %s "
                " and rule_id = %s and from_vocabulary_id = %s and  from_concept_code = %s;")

        self._cur.execute(stmt, (extract_study_id, function_name, long_name, rule_id, from_vocabulary_id, from_concept_code))
        rows = self._cur.fetchall()
        return rows

    def add_export_mapping_rows(self, criteria_row):

        study_id = self.get_study_id(criteria_row['Study'])
        extract_study_id = study_id + self.EXTRACT_ID_OFFSET
        if self.get_extract_study(extract_study_id) is None:
            self.create_extract_study(extract_study_id, "NEW extract STUDY")
        stmt = ("insert into categorization_function_metadata"
                "(extract_study_id, function_name, long_name, rule_id,"
                " from_vocabulary_id, from_concept_code, comment, from_table, short_name)"
                " values (%s, %s, %s,  %s, %s, %s,  %s, %s, %s)")

        (vocabulary_id, concept_code, to_table) = self.lookup_by_cui(criteria_row['Phenotype_cui'])
        (function_name, to_column) = self.infer_function_and_column(
            to_table,
            criteria_row['Lower_bound'], criteria_row['Upper_bound'],
            criteria_row['Match_number'], criteria_row['Match_string'])
        to_column = self._type_map[criteria_row['Value_type']]; # infer
        long_name = criteria_row['Phenotype']
        key = (extract_study_id, function_name, long_name, 1, vocabulary_id, concept_code)
        if key not in self._mapping_keys:
            self._cur.execute(stmt, (extract_study_id, function_name,  long_name, 1,
            vocabulary_id, concept_code, '', to_table, long_name[0:9]))
        #else:
        #    print("not putting an export mapping because I already have the key", key)

# EXPORT MAPPING ARGUMENTS
    def get_export_mapping_arguments(self, extract_study_id, function_name, long_name, rule_id, rank):
        stmt = ("select extract_study_id, function_name, long_name, rule_id, NULLIF(value_limit, 999), "
                "NULLIF(rank, 999), from_string, from_concept_id"
                " from categorization_function_parameters"
                " where extract_study_id = %s and function_name = %s"
                " and long_name = %s and rule_id = %s and rank=%s"
                " order by extract_study_id, rank")

        self._cur.execute(stmt, (extract_study_id, function_name, long_name, rule_id, rank))
        rows = self._cur.fetchall()
        return rows

    def get_export_mapping_arguments_all(self):
        stmt = ("select extract_study_id, function_name, long_name, rule_id, value_limit, "
                "rank, from_string, from_concept_id"
                " from categorization_function_parameters"
                " order by extract_study_id, rank")

        self._cur.execute(stmt)
        rows = self._cur.fetchall()
        return rows

    def add_export_mapping_arguments(self, criteria_row):
        extract_study_id = self.EXTRACT_ID_OFFSET +  self.get_study_id(criteria_row['Study'])
        (value_vocabulary_id, value_concept_code, to_table) = self.lookup_by_cui(criteria_row['Indicates_cui'])
        (function_name, to_column) = self.infer_function_and_column(
            to_table,
            criteria_row['Lower_bound'], criteria_row['Upper_bound'], 
            criteria_row['Match_number'], criteria_row['Match_string'])
        to_column = self._type_map[criteria_row['Value_type']]; # infer
        long_name = criteria_row['Phenotype']
        rule_id = 1
        rank = criteria_row['Indicates_rank']
        value_limit = criteria_row['Upper_bound']
        from_string = criteria_row['Match_string']
        self.put_export_mapping_arguments(extract_study_id, function_name, long_name, rule_id, rank,
                                          value_vocabulary_id, value_concept_code, value_limit, from_string)

    def put_export_mapping_arguments(self, extract_study_id, function_name, long_name, rule_id, rank,
                                         from_vocabulary_id, from_concept_code, value_limit, from_string):

        #print("DEBUG", extract_study_id, function_name, long_name, rule_id, rank,
        #    from_vocabulary_id, from_concept_code, value_limit, from_string)

        stmt = ("insert into categorization_function_parameters"
                "(extract_study_id, function_name, long_name, rule_id,"
                " value_limit, rank, from_string) "
                " values (%s, %s, %s,  %s,   %s, %s, %s)")
        stmt_with_concept = ("insert into categorization_function_parameters"
                "(extract_study_id, function_name, long_name, rule_id,"
                " value_limit, rank, from_string, from_concept_id) "
                " values (%s, %s, %s,  %s, %s, %s,  %s, "
                " (select concept_id from concept c where c.vocabulary_id = %s and c.concept_code = %s ));")
        if from_concept_code is not None:
            self._cur.execute(stmt_with_concept, (extract_study_id, function_name,  long_name, 1,

            value_limit, rank, from_string, from_vocabulary_id, from_concept_code))
        else:
            if (math.isnan(value_limit)):
                value_limit = None
            if (math.isnan(rank)):
                rank = 1
            self._cur.execute(stmt, (extract_study_id, function_name,  long_name, 1, value_limit, rank, from_string))



def main(filename):
    ''' 
        load_studies.py loads studies configured in the study table.
        This is restrcited to reading the csv files and creating tables for them.
        study_name can be None, or 'ALL', or the specific name of a single study
    '''

    #DBNAME=os.environ.get('PGDATABASE')
    #DBNAME='heart_db_v5'
    DBNAME='test_install_3'
    USERNAME=os.environ.get('PGUSER')
    PASSWORD=os.environ.get('PGPASSWORD')
    HOST=os.environ.get('PGHOST')
    PORT=os.environ.get('PGPORT')
    logger = logging.getLogger(__name__)

    con = psycopg2.connect(database=DBNAME, user=USERNAME)
    con.autocommit=True;

    df = pandas.read_csv(encoding='latin1', filepath_or_buffer=filename, na_values=['\\N'])
    # Index(['Study', 'Data_form', 'Data_Field', 'Phenotype', 'Phenotype_cui',
    #       'Lower_bound', 'Data_Units', 'Upper_bound', 'Match_number',
    #       'Match_string', 'Gender_spec', 'Indicates_rank', 'string_value'],
    #       dtype='object')
    df_columns_set = set(df.columns)
    if 'Value_type' not in df_columns_set:
        print("file is lacking a Value_type column. it must be pre-processed.")
        exit()
    if 'Indicates_cui' not in df_columns_set
        print("file is lacking a Indicates_cui column. it must be pre-processed.")
        exit()

    importer = CriteriaSetImporter(con)
    id = [0]
    id[0] = importer.find_max_study_id() # pass-by-reference!!!
    def lamedah(r, id):
        importer.create_new_study(r, id[0])
        id[0] = id[0] + 1
    df.apply(lambda row:  lamedah(row['Study'], id), axis=1)

    # insert/compare mapping rows
    def lamedah(criteria_row):
        importer.add_study_mapping_row(criteria_row)
        importer.add_study_mapping_arguments(criteria_row)
        importer.add_export_mapping_rows(criteria_row)
        importer.add_export_mapping_arguments(criteria_row)
    df.apply(lambda row: lamedah(row), axis=1)


if __name__ == '__main__':
    parser = argh.ArghParser()
    argh.set_default_command(parser, main)
    argh.dispatch(parser)
