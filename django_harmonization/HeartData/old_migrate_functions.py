           return None
 
#############################################################################################
from HeartData.concepts import AFRICAN_AMERICAN_CONCEPT_ID 
from HeartData.concepts import CAUCASIAN_CONCEPT_ID
from HeartData.concepts import ASIAN_CONCEPT_ID
from HeartData.concepts import PACIFIC_ISLANDER_CONCEPT_ID
from HeartData.concepts import AMERICAN_INDIAN_CONCEPT_ID
from HeartData.concepts import HISPANIC_CONCEPT_ID
from HeartData.concepts import OTHER_RACE_CONCEPT_ID
from HeartData.concepts import MALE_CONCEPT_ID
from HeartData.concepts import FEMALE_CONCEPT_ID 
from HeartData.concepts import YES_CONCEPT_ID
from HeartData.concepts import NO_CONCEPT_ID

from HeartData.concepts import YES_CONCEPT_CODE
from HeartData.concepts import YES_VOCABULARY_ID
from HeartData.concepts import NO_CONCEPT_CODE
from HeartData.concepts import NO_VOCABULARY_ID
 
from HeartData.concepts import SMOKE_NEVER_CONCEPT_ID
from HeartData.concepts import SMOKE_CURRENT_CONCEPT_ID
from HeartData.concepts import SMOKE_SMOKELESS_CONCEPT_ID
from HeartData.concepts import SMOKE_FORMER_CONCEPT_ID
from HeartData.concepts import BARI2D_RACE_1_CONCEPT_ID
from HeartData.concepts import BARI2D_RACE_2_CONCEPT_ID

# ---- BEST -----
BEST_MALE=1
BEST_FEMALE = 2

BEST_YES=1
BEST_NO=2
 
 
# -- PARADIGM --
P_YES=1
P_NO=0
 
P_MALE=1
P_FEMALE=2
 
P_CAUCASIAN =1
P_AFRICAN_AMERICAN=2
P_ASIAN=3
P_OTHER=4
 
## TBD: ATMOSPHERE TODO
A_CAUCASIAN =1
A_AFRICAN_AMERICAN=2
A_ASIAN=3
A_NATIVE_AMERICAN=7
A_PAC_ISLAND=8
A_OTHER=88


def best_sex_to_concept(best_sex):
    """ Maps the value, work in concert with a mapping in study_to_ohdsi_mapping
       that specifies the destination vocabulary and concept_code. The value's term
       is in the same vocabulary and calculated here.
    """
    # concept: SNOMED 263495000
    # value: Female       |   703118005 | SNOMED         | Answer
    # value: Male         |   703111700 | SNOMED         | Answer
BEST_MALE=1
BEST_FEMALE = 2
    if best_sex == BEST_MALE:
        return 45766034 #703117000  # male
    elif best_sex == BEST_FEMALE:
        return 45766035 #703118005  # female
    else:
        return None


def best_race_to_concept(best_race):
    # value: African American   SNOMED 15086000
    # value: Caucasian          SNOMED 413773004
    # value: hispanic           SNOMED 414408004
    # value: asian              SNOMED 413582008
    # value: native am.         SNOMED 413490006
    # value: other ?
    # concept:  SNOMED Race          | 103579009
    # BEST: 1:White, 2:Black, 3:Hispanic, 4:Asian, 5:Native Am., 6:Other
    race_mapping = {1:4185154, 2:4035165, 3:4188159, 4:4211353, 5:4184966, 6:0}
    return race_mapping[best_race]

def best_ethnicity_to_concept(best_race):
    # value: Hispanic or Latino     |   38003563 | Ethnicity     | Ethnicity
    # value: Not Hispanic or Latino |   38003564 | Ethnicity     | Ethnicity
    # OHDSI concept ids from a standard concept, no particular vocabulary?
    ethnicity_mapping = { 1:38003564, 2:38003564, 3:38003563, 4:38003564, 5:38003564, 6:38003564}
    return ethnicity_mapping[best_race]


