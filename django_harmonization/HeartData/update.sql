UPDATE study_mapping_arguments  sma
SET concept_code = m.concept_code , vocabulary_id = m.vocabulary_id
FROM study_mapping_arguments a 
    JOIN study_to_ohdsi_mapping m ON  a.study_id = m.study_id
        AND a.from_table = m.from_table 
        AND a.from_column = m.from_column 
        AND a.from_where_clause = m.from_where_clause 
        AND a.from_where_column = m.from_where_column 
WHERE sma.study_id = m.study_id
  AND sma.from_table = m.from_table 
  AND sma.from_column = m.from_column 
  AND sma.from_where_clause = m.from_where_clause 
  AND sma.from_where_column = m.from_where_column ;

